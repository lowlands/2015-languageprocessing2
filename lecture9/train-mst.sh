#!/bin/bash
## train MST on the specified training file
TRAIN=en-ud-train.conllu
java -cp mstparser.jar:lib/trove.jar mstparser.DependencyParser train train-file:$TRAIN model-name:mst.model